# ROS WorkSpace Example



this is an example ros workspace project. we are starting with create this workspace.



## init workspace

to init a workspace is very simple, first you should make a dir, we say test_ws:

```
mkdir test_ws
```

then we cd that dir, and run `catkin_init_workspace`:

```
source /opt/ros/kinetic/setup.bash
cd test_ws
catkin_init_workspace
```

the you will get an init workspace, but if you want to using this example directly, you just need git clone this, and source your setup script file.



## send image through topic

we are using a simple detector, get image frame from camera, and then send the detected result to a topic we called: `perception/detector`, the detector code is just like this:

```
#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"


void imageCallBack(const sensor_msgs::ImageConstPtr& tem_msg) {
    try
    {
        ROS_INFO_STREAM_ONCE("oh, fuck, I received a processed image back!!!!!!!!!!!!!!!!");
        cv::imshow("after post processed image", cv_bridge::toCvShare(tem_msg, "bgr8")->image);
        cv::waitKey(30);
    }
    catch (cv_bridge::Exception &e)
    {
        ROS_ERROR("could not convert from '%s' to 'mono8'", tem_msg->encoding.c_str());
    }
    
}



int main(int argc, char *argv[])
{
    
    ros::init(argc, argv, "detector");
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("perception/detector", 1);
    image_transport::Subscriber sub = it.subscribe("perception/post_process", 1, imageCallBack);

    cv::Mat image = cv::imread("/home/robox/Pictures/dog.jpeg", cv::CAP_MODE_RGB);

    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
    ros::Rate loop_rate(10);
    while (nh.ok())
    {
        ROS_INFO_STREAM("I send a message of image.");
        pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
```

this will send a message reading from local and send very 0.1 seconds. Here we also have a executor to receive that message:

```
#include "ros/ros.h"    
#include "image_transport/image_transport.h"    
#include "cv_bridge/cv_bridge.h"    
#include "sensor_msgs/image_encodings.h"    
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"    
#include <iostream>    
    
void imageCallBack(const sensor_msgs::ImageConstPtr& msg) {
    // here we will receive a message
    try
    {
        ROS_INFO_STREAM("oh, fuck, I am executor and I recevied an image from detector!!!!");
        cv::imshow("fuck image executor received.", cv_bridge::toCvShare(msg, "bgr8")->image);
        cv::waitKey(1);
    }
    catch (cv_bridge::Exception &e)
    {
        /* code for Catch */
        ROS_ERROR("failded to open received message of image. error: %s", msg->encoding.c_str());
    }
    
}
    
int main(int argc, char** argv)    
{    
    ros::init(argc, argv, "just_a_test_node"); 
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);
    image_transport::Subscriber sub = it.subscribe("perception/detector", 1, imageCallBack);
    ROS_INFO_STREAM("Did I executor got the image???");

    ros::Rate loop_rate(10);
    while (nh.ok())
    {
        ROS_INFO_STREAM("I am executor, I am receving image!!!");
        ros::spinOnce();
        loop_rate.sleep();
    }
    
        
    return 0;    
}  
```

you should write your CMakeLists.txt in every package and found opencv.