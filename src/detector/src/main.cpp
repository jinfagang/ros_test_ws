#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"


void imageCallBack(const sensor_msgs::ImageConstPtr& tem_msg) {
    try
    {
        ROS_INFO_STREAM_ONCE("oh, fuck, I received a processed image back!!!!!!!!!!!!!!!!");
        cv::imshow("after post processed image", cv_bridge::toCvShare(tem_msg, "bgr8")->image);
        cv::waitKey(30);
    }
    catch (cv_bridge::Exception &e)
    {
        ROS_ERROR("could not convert from '%s' to 'mono8'", tem_msg->encoding.c_str());
    }
    
}



int main(int argc, char *argv[])
{
    
    ros::init(argc, argv, "detector");
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub = it.advertise("perception/detector", 1);
    image_transport::Subscriber sub = it.subscribe("perception/post_process", 1, imageCallBack);

    cv::Mat image = cv::imread("/home/robox/Pictures/dog.jpeg", cv::CAP_MODE_RGB);

    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", image).toImageMsg();
    ros::Rate loop_rate(10);
    while (nh.ok())
    {
        ROS_INFO_STREAM("I send a message of image.");
        pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
    }
    


    return 0;
}


