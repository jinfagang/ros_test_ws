#include "std_msgs/String.h"
#include <sstream>
#include "ros/ros.h"
#include "ros/time.h"
#include "tutor/Command.h"




int main(int argc, char *argv[])
{
    ros::init(argc, argv, "example_node");
    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<tutor::Command>("command", 1000);
    ros::Rate loop_rate(10);

    while (ros::ok())
    {
        tutor::Command commandMessage;
        commandMessage.messageContent = "this is content";
        commandMessage.speedLeft = 23.;
        commandMessage.speedRight = 34.;

        chatter_pub.publish(commandMessage);

        ROS_INFO("%s", commandMessage.messageContent.c_str());
        ros::spinOnce();
        loop_rate.sleep();
    }
    


    return 0;
}