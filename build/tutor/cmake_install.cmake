# Install script for directory: /media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/tutor/msg" TYPE FILE FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor/msg/Command.msg")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/tutor/cmake" TYPE FILE FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build/tutor/catkin_generated/installspace/tutor-msg-paths.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel/include/tutor")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel/share/roseus/ros/tutor")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel/share/common-lisp/ros/tutor")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel/share/gennodejs/ros/tutor")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND "/usr/bin/python" -m compileall "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel/lib/python2.7/dist-packages/tutor")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel/lib/python2.7/dist-packages/tutor")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build/tutor/catkin_generated/installspace/tutor.pc")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/tutor/cmake" TYPE FILE FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build/tutor/catkin_generated/installspace/tutor-msg-extras.cmake")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/tutor/cmake" TYPE FILE FILES
    "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build/tutor/catkin_generated/installspace/tutorConfig.cmake"
    "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build/tutor/catkin_generated/installspace/tutorConfig-version.cmake"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/tutor" TYPE FILE FILES "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor/package.xml")
endif()

