# CMake generated Testfile for 
# Source directory: /media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src
# Build directory: /media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("detector")
subdirs("executor")
subdirs("tutor")
