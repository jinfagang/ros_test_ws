// Auto-generated. Do not edit!

// (in-package tutor.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Command {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.messageContent = null;
      this.speedLeft = null;
      this.speedRight = null;
    }
    else {
      if (initObj.hasOwnProperty('messageContent')) {
        this.messageContent = initObj.messageContent
      }
      else {
        this.messageContent = '';
      }
      if (initObj.hasOwnProperty('speedLeft')) {
        this.speedLeft = initObj.speedLeft
      }
      else {
        this.speedLeft = 0.0;
      }
      if (initObj.hasOwnProperty('speedRight')) {
        this.speedRight = initObj.speedRight
      }
      else {
        this.speedRight = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Command
    // Serialize message field [messageContent]
    bufferOffset = _serializer.string(obj.messageContent, buffer, bufferOffset);
    // Serialize message field [speedLeft]
    bufferOffset = _serializer.float32(obj.speedLeft, buffer, bufferOffset);
    // Serialize message field [speedRight]
    bufferOffset = _serializer.float32(obj.speedRight, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Command
    let len;
    let data = new Command(null);
    // Deserialize message field [messageContent]
    data.messageContent = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [speedLeft]
    data.speedLeft = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [speedRight]
    data.speedRight = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.messageContent.length;
    return length + 12;
  }

  static datatype() {
    // Returns string type for a message object
    return 'tutor/Command';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '8d747c3cc1a1dad0abafa278f8e8b84b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string messageContent
    float32 speedLeft
    float32 speedRight
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Command(null);
    if (msg.messageContent !== undefined) {
      resolved.messageContent = msg.messageContent;
    }
    else {
      resolved.messageContent = ''
    }

    if (msg.speedLeft !== undefined) {
      resolved.speedLeft = msg.speedLeft;
    }
    else {
      resolved.speedLeft = 0.0
    }

    if (msg.speedRight !== undefined) {
      resolved.speedRight = msg.speedRight;
    }
    else {
      resolved.speedRight = 0.0
    }

    return resolved;
    }
};

module.exports = Command;
