// Generated by gencpp from file tutor/Command.msg
// DO NOT EDIT!


#ifndef TUTOR_MESSAGE_COMMAND_H
#define TUTOR_MESSAGE_COMMAND_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace tutor
{
template <class ContainerAllocator>
struct Command_
{
  typedef Command_<ContainerAllocator> Type;

  Command_()
    : messageContent()
    , speedLeft(0.0)
    , speedRight(0.0)  {
    }
  Command_(const ContainerAllocator& _alloc)
    : messageContent(_alloc)
    , speedLeft(0.0)
    , speedRight(0.0)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _messageContent_type;
  _messageContent_type messageContent;

   typedef float _speedLeft_type;
  _speedLeft_type speedLeft;

   typedef float _speedRight_type;
  _speedRight_type speedRight;




  typedef boost::shared_ptr< ::tutor::Command_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::tutor::Command_<ContainerAllocator> const> ConstPtr;

}; // struct Command_

typedef ::tutor::Command_<std::allocator<void> > Command;

typedef boost::shared_ptr< ::tutor::Command > CommandPtr;
typedef boost::shared_ptr< ::tutor::Command const> CommandConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::tutor::Command_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::tutor::Command_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace tutor

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'tutor': ['/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::tutor::Command_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::tutor::Command_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::tutor::Command_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::tutor::Command_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::tutor::Command_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::tutor::Command_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::tutor::Command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "8d747c3cc1a1dad0abafa278f8e8b84b";
  }

  static const char* value(const ::tutor::Command_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x8d747c3cc1a1dad0ULL;
  static const uint64_t static_value2 = 0xabafa278f8e8b84bULL;
};

template<class ContainerAllocator>
struct DataType< ::tutor::Command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "tutor/Command";
  }

  static const char* value(const ::tutor::Command_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::tutor::Command_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string messageContent\n\
float32 speedLeft\n\
float32 speedRight\n\
";
  }

  static const char* value(const ::tutor::Command_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::tutor::Command_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.messageContent);
      stream.next(m.speedLeft);
      stream.next(m.speedRight);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Command_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::tutor::Command_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::tutor::Command_<ContainerAllocator>& v)
  {
    s << indent << "messageContent: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.messageContent);
    s << indent << "speedLeft: ";
    Printer<float>::stream(s, indent + "  ", v.speedLeft);
    s << indent << "speedRight: ";
    Printer<float>::stream(s, indent + "  ", v.speedRight);
  }
};

} // namespace message_operations
} // namespace ros

#endif // TUTOR_MESSAGE_COMMAND_H
