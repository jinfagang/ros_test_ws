;; Auto-generated. Do not edit!


(when (boundp 'tutor::Command)
  (if (not (find-package "TUTOR"))
    (make-package "TUTOR"))
  (shadow 'Command (find-package "TUTOR")))
(unless (find-package "TUTOR::COMMAND")
  (make-package "TUTOR::COMMAND"))

(in-package "ROS")
;;//! \htmlinclude Command.msg.html


(defclass tutor::Command
  :super ros::object
  :slots (_messageContent _speedLeft _speedRight ))

(defmethod tutor::Command
  (:init
   (&key
    ((:messageContent __messageContent) "")
    ((:speedLeft __speedLeft) 0.0)
    ((:speedRight __speedRight) 0.0)
    )
   (send-super :init)
   (setq _messageContent (string __messageContent))
   (setq _speedLeft (float __speedLeft))
   (setq _speedRight (float __speedRight))
   self)
  (:messageContent
   (&optional __messageContent)
   (if __messageContent (setq _messageContent __messageContent)) _messageContent)
  (:speedLeft
   (&optional __speedLeft)
   (if __speedLeft (setq _speedLeft __speedLeft)) _speedLeft)
  (:speedRight
   (&optional __speedRight)
   (if __speedRight (setq _speedRight __speedRight)) _speedRight)
  (:serialization-length
   ()
   (+
    ;; string _messageContent
    4 (length _messageContent)
    ;; float32 _speedLeft
    4
    ;; float32 _speedRight
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _messageContent
       (write-long (length _messageContent) s) (princ _messageContent s)
     ;; float32 _speedLeft
       (sys::poke _speedLeft (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _speedRight
       (sys::poke _speedRight (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _messageContent
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _messageContent (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _speedLeft
     (setq _speedLeft (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _speedRight
     (setq _speedRight (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get tutor::Command :md5sum-) "8d747c3cc1a1dad0abafa278f8e8b84b")
(setf (get tutor::Command :datatype-) "tutor/Command")
(setf (get tutor::Command :definition-)
      "string messageContent
float32 speedLeft
float32 speedRight
")



(provide :tutor/Command "8d747c3cc1a1dad0abafa278f8e8b84b")


