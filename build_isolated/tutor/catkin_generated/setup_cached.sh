#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/tutor:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/tutor/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/tutor/lib/pkgconfig:$PKG_CONFIG_PATH"
export ROSLISP_PACKAGE_DIRECTORIES="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/tutor/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor:$ROS_PACKAGE_PATH"