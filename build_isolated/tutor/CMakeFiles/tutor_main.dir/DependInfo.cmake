# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor/src/main.cpp" "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build_isolated/tutor/CMakeFiles/tutor_main.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"tutor\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/tutor/include"
  "/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/tutor/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
