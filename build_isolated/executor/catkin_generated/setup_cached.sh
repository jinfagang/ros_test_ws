#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/executor:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/executor/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/executor/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/build_isolated/executor"
export ROSLISP_PACKAGE_DIRECTORIES="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/devel_isolated/executor/share/common-lisp"
export ROS_PACKAGE_PATH="/media/robox/Netac/CodeSpace/ng/auto_car/ros_workspace/test_ws/src/executor:$ROS_PACKAGE_PATH"